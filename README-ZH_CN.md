<div>
    <p align="center">
        <img width="300" src="https://cdn.jsdelivr.net/gh/inlang/monorepo/inlang/assets/logo-white-background.png"/>
    </p>
    <h2 align="center">
        全球通用的软件基础设施
    </h2>
</div>

<div align="center">
  <a href="./README-ZH_CN.md"><img alt="简体中文" src="https://img.shields.io/badge/%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87-Simplified%20Chinese-blue" /></a>
</div>

<p align="center">
  <a href='https://discord.gg/gdMPPWy57R' target="_blank"><img alt='加入 Discord 社区' src='https://img.shields.io/badge/Discord-100000?style=flat&logo=Discord&logoColor=white&labelColor=5865F2&color=5865F2'/></a>
  <a href='https://twitter.com/inlangHQ' target="_blank"><img alt='在 Twitter 上关注 inlang' src='https://img.shields.io/badge/Twitter-100000?style=flat&logo=Twitter&logoColor=white&labelColor=1A8CD8&color=1A8CD8'/></a>
</p>

## 社区与支持

**[📝 文档](https://inlang.com/documentation)** - 快速入门

**[🌱 生态系统](https://inlang.com/marketplace)** - 插件、资源

**[🛠️ SDKs](https://inlang.com/documentation/sdk)** - SDK 支持

**[🚩 Issues](https://github.com/inlang/monorepo/issues)** - 反馈使用过程遇到的问题或缺陷

**[💬 讨论](https://github.com/inlang/monorepo/discussions)** - 寻求帮助、提出问题或特性，共同探讨 inlang 的发展。

**[🗣️ Discord](https://discord.gg/gdMPPWy57R)** - 与社区一起交流

## 应用形态

Inlang 由多个应用程序组成，这些应用程序可以单独使用，也可以相互组合使用。

### [编辑器](https://inlang.com/marketplace/app.inlang.editor)

在简洁高效的 Web 界面中管理翻译并将更改推送到你的存储库。

✅ 适用于现有翻译文件</br>
✅ 完美融合 Git 的协作流程，例如 Pull Request</br>
✅ 无须额外部署，无须同步流水线</br>
✅ 无须额外的帐户集成</br>

![用于管理翻译的 inlang 编辑器](https://github.com/inlang/monorepo/assets/59048346/85cfee69-96da-4b2d-8e34-a0b7abc72212)

### [IDE-Extension](https://inlang.com/marketplace/app.inlang.ideExtension)

通过 VSCode 扩展，你可以在开发环境中直接完成翻译工作。

✅ 一键从代码提取翻译消息</br>
✅ 通过代码内联注解，提供翻译结果查阅</br>
✅ 完备的翻译规范检查、错误提示和预警</br>

![VSCode 扩展内联注解](https://cdn.jsdelivr.net/gh/inlang/monorepo/inlang/assets/ide-extension/inline.gif)

### [CLI](https://inlang.com/marketplace/app.inlang.cli)

[@inlang/cli](https://github.com/inlang/monorepo/tree/main/inlang/source-code/cli) 是一个命令行 (CLI) 工具，可让你与 Inlang 基础设施进行交互。

✅ 初始化并验证配置</br>
✅ 翻译规范检查</br>
✅ 支持机翻集成</br>
✅ 启动并打开 Inlang Web 编辑器</br>
✅ 支持与持续集成/持续部署 (CI/CD) 集成自动化</br>

![CLI validate example](https://github.com/inlang/monorepo/assets/59048346/44fcaf1e-aff4-4533-b973-1690fc6db93e)
